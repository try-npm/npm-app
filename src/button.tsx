import * as React from "react";

export const Button = ({props}: { props: any }) => {
    return (
        <button className={props.className}
                data-id={props.id}
                type={props.type}
                name={props.name}
                value={props.value}
                disabled={props.disabled}
                onClick={props.handleClick}>
            <h4>{props.label}</h4>
        </button>
    )
}